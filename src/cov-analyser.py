# -*- coding: utf-8 -*-
#! /usr/bin/env python3
#matplotlib inline

from typing import Union, List

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import warnings

def date_from_string(date_string: Union[str, pd.Series]) -> Union[pd.Timestamp, pd.Series]:
    """Parse date string(s) to Pandas Timestamps.

    Args:
        date_string: Date string(s) in the format ``%d/%b/%Y``.

    Example:
        A valid input string is ``12 Mar 2018 07:00:00``.

    Returns:
        The parsed Timestamp object(s).

    """
    return pd.to_datetime(date_string, format='%m/%d/%y', exact=False, utc=False)

class CovidContainer():
    """ A result object
    
    Constructor:
        pNameFilter: directory where filter for list of countries is located
        fNameFilter: csv file containing the list of countries
        pNameData: directory where time data series are located
        fNameData: filename to load during initialization of the object
                   this filename is the descriptor of the studied cases
        debug: a boolean flag that when turned to "True" will printout to Console
               some debugging data
    
    Properties:
        self.df0: Stores the Pandas dataframe of list of countries
        self.df1: Stores the Pandas dataframe with country/region data
                  of list of countries filtered
        self.df2T: Stores the Pandas dataframe transposed of studied cases data
                  of list of countries filtered
    """

    def __init__(self, pNameFilter: str='./db/inputs/',
        fNameFilter: str='list_countries_interest.csv',
        pNameData: str='./db/inputs/COVID-19/csse_covid_19_data/csse_covid_19_time_series/',
        fNameData: str='time_series_19-covid-Confirmed.csv', debug: bool=False) -> None:
        # Store config
        self.debug = debug
        self.pNameFilter = pNameFilter
        self.fNameFilter = fNameFilter
        self.pNameData = pNameData
        self.fNameData = fNameData
        self.pNameResults = './db/outputs/'

        # Check type of data and treat differently the 2 approaches they had for countries with overseas
        df_confirmed_cases = pd.read_csv(pNameData + fNameData)
        test_case_on_france = ~df_confirmed_cases[
            df_confirmed_cases["Country/Region"] =="France"]["Province/State"].notnull()
        flag_correct_list_of_countries = False
        for value in test_case_on_france.values:
            if value == True:
                flag_correct_list_of_countries = True
                break

        # Read from file
        df_list_countries = pd.read_csv(pNameFilter + fNameFilter)
        if flag_correct_list_of_countries:
            NaN = np.nan
            df_list_countries.loc[
                df_list_countries["Country/Region"] == "France", "Province/State"] = NaN
            df_list_countries.loc[
                df_list_countries["Country/Region"] == "United Kingdom", "Province/State"] = NaN
            df_list_countries.loc[
                df_list_countries["Country/Region"] == "Netherlands", "Province/State"] = NaN
            df_list_countries.loc[
                df_list_countries["Country/Region"] == "Denmark", "Province/State"] = NaN

        if debug:
            print(df_list_countries.head(20))
            print(df_list_countries.shape)

        # Check for Europe main countries
        ## Keep only the countries present in the list for analysis
        df1 = df_confirmed_cases[df_confirmed_cases["Country/Region"].isin(df_list_countries["Country/Region"])]
        ## Keep the NaN (basically keep European countries that have no overseas territories)
        df2a = df1[~df1["Province/State"].notnull()]
        ## Suppress the NaN (so China + the countries that have oversea territories + oversea territories themselve)
        df2b = df1[df1["Province/State"].notnull()]
        ## Recover only the data on European grounds for countries with oversea territories
        ## To allow the merge at the end, the trick is to add the name of the country on the corresponding
        # Province/State cell
        df2c = df2b[df2b["Province/State"]==df2b["Country/Region"]]

        # Recover for china
        df3 = df_confirmed_cases[df_confirmed_cases["Country/Region"]=="China"]

        # Concatenate European ground countries and China
        df4 = pd.concat([df2a, df2c, df3])

        if debug:
            print("df1")
            print(df1)
            print(df1.shape)
            print("df2a")
            print(df2a)
            print(df2a.shape)
            print("df2b")
            print(df2b)
            print(df2b.shape)
            print("df2c")
            print(df2c)
            print(df2c.shape)
            print("df3")
            print(df3)
            print(df3.shape)
            print("df4")
            print(df4)
            print(df4.shape)

        # Merge databases to recover population and area filed
        df5 = df_list_countries.merge(df4)
        df5['Id-Country'] = df5.apply(
            lambda row: row['Area']+'-'+row['Country/Region']+
            '-'+str(row['Province/State']), axis=1)
        # Reorganise columns
        colnames = df5.columns.tolist()
        colnames = colnames[0:6] + colnames[-1:] + colnames[7:-1]
        dfM_confirmed_cases = df5[colnames]

        # Split in two
        dfM1_confirmed_cases = dfM_confirmed_cases[colnames[0:7]]
        dfM2_confirmed_cases = dfM_confirmed_cases[colnames[6:]]
        dfM2T_confirmed_cases = dfM2_confirmed_cases.T
        # Transform indexes and column names
        colnames = dfM2T_confirmed_cases.iloc[0]
        colnames.name = 'Date String'
        dfM2T_confirmed_cases.columns = colnames
        dfM2T_confirmed_cases = dfM2T_confirmed_cases.drop([dfM2T_confirmed_cases.index[0]])
        dfM2T_confirmed_cases.index = [date_from_string(tmpDate) for tmpDate in dfM2T_confirmed_cases.index.values]
        dates = dfM2T_confirmed_cases.index.values
        if debug:
            print(dates)
            print(dfM2T_confirmed_cases)
        
        self.df0 = df_list_countries
        self.df1 = dfM1_confirmed_cases
        self.df2T = dfM2T_confirmed_cases
    
    def calculate_totals(self, area_name: str='Asia', country_name: str='All') -> None:
        # Total cases per area
        tmp_df1 = self.df1[self.df1['Area']==area_name]
        if not(country_name=='All'):
            tmp_df1 = tmp_df1[tmp_df1['Country/Region']==country_name]
        tmp_df2T = self.df2T[tmp_df1['Id-Country']]
        tmp = np.sum(tmp_df2T, axis=1)
        self.df2T['Total-'+area_name+'-'+country_name] = tmp
        # Total population per area
        tot_population = np.sum(tmp_df1['Population'], axis=0)
        average_lat = np.average(tmp_df1['Lat'], axis=0)
        average_long = np.average(tmp_df1['Long'], axis=0)
        new_row = {'Province/State': area_name, 'Area': area_name,
            'Country/Region': area_name, 'Population': tot_population,
            'Lat': average_lat, 'Long': average_long,
            'Id-Country': 'Total-'+area_name+'-'+country_name}
        self.df1 = self.df1.append(new_row, ignore_index = True)
        return

    def calculate_gradient(self) -> None:
        # DF centrée
        self.df2TG = 0.5*(-self.df2T.diff(axis=0, periods=-1)+self.df2T.diff(axis=0, periods=1))
        # Smooth using rooling window
        self.df2TGS1 = self.df2TG.rolling(window=3, min_periods=3, win_type='blackmanharris').sum()
        self.df2TGS2 = self.df2TG.rolling(window=7, min_periods=3, win_type='blackmanharris').sum()
        #self.df2TGS1 = self.df2TG.rolling(2, win_type='gaussian').sum(std=3)
        #self.df2TGS1 = self.df2TG.rolling(2, win_type='triang').sum()
        # Replace the NaN by 0
        self.df2TGS1 = self.df2TGS1.fillna(0)
        self.df2TGS2 = self.df2TGS2.fillna(0)
        self.df2TG = self.df2TG.fillna(0)
        # Correct effect of roolwing window on the "area" results
        for column_name in self.df2TGS1.columns:
            initial_sum = self.df2TG[column_name].sum()
            final_sum = self.df2TGS1[column_name].sum()
            try:
                factor = initial_sum / final_sum
            #except RuntimeWarning:
            except Exception:
                print("Exception occured for column: " + column_name)
            self.df2TGS1[column_name] = factor * self.df2TGS1[column_name]
        for column_name in self.df2TGS2.columns:
            initial_sum = self.df2TG[column_name].sum()
            final_sum = self.df2TGS2[column_name].sum()
            try:
                factor = initial_sum / final_sum
            #except RuntimeWarning:
            except Exception:
                print("Exception occured for column: " + column_name)
            self.df2TGS2[column_name] = factor * self.df2TGS2[column_name]
        
        # 2nd derived quantity -> meaningless here because too noisy and to badly sampled
        # DF centrée
        self.df2TG2 = 0.5*(-self.df2TGS2.diff(axis=0, periods=-1)+self.df2TGS2.diff(axis=0, periods=1))
        self.df2TG2 = self.df2TG2.fillna(0)
        return

    def dump_data(self, flag_normalized = False) -> None:
        if flag_normalized:
            self.df0.to_csv(self.pNameResults+'dfM0'+self.fNameData)
            self.df1.to_csv(self.pNameResults+'dfM1'+self.fNameData)
            self.df2T.to_csv(self.pNameResults+'dfM2T'+self.fNameData)
            self.df2TG.to_csv(self.pNameResults+'dfM2TG'+self.fNameData)
            self.df2TGS1.to_csv(self.pNameResults+'dfM2TGS1'+self.fNameData)
            self.df2TGS2.to_csv(self.pNameResults+'dfM2TGS2'+self.fNameData)
            self.df2TG2.to_csv(self.pNameResults+'dfM2TG2'+self.fNameData)
        else:
            self.df2T.to_csv(self.pNameResults+'dfM2T-norm'+self.fNameData)
            self.df2TG.to_csv(self.pNameResults+'dfM2TG-norm'+self.fNameData)
            self.df2TGS1.to_csv(self.pNameResults+'dfM2TGS1-norm'+self.fNameData)
            self.df2TGS2.to_csv(self.pNameResults+'dfM2TGS2-norm'+self.fNameData)
            self.df2TG2.to_csv(self.pNameResults+'dfM2TG2-norm'+self.fNameData)
        return
    
    def normalize_on_population(self, direction=True) -> None:
        df1_tmp = self.df1
        df1_tmp = df1_tmp.set_index('Id-Country')
        print('Normalization by :')
        for column_name in self.df2T.columns:
            print('Population of ' + column_name + ' equals to:' + str(df1_tmp.loc[column_name, 'Population']))
            if direction:
                self.df2T[column_name] = 100 * self.df2T[column_name] / df1_tmp.loc[column_name, 'Population']
            else:
                self.df2T[column_name] = 0.01 * self.df2T[column_name] * df1_tmp.loc[column_name, 'Population']
        # Gradients must be updated
        self.calculate_gradient()

    
    # Check doc
    # https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.plot.html?highlight=plot#pandas.DataFrame.plot
    def plot_data(self,
        list_to_plot: list()=['Total-Asia-China', 'Total-Europe-All'],
        log_flag: bool=False, suffix_name = '') -> None:
        if log_flag:
            fName_compl = '_log'
        else:
            fName_compl = ''
        # Create plot, leveraging matplotlib, from pandas object
        plot_df2T = self.df2T.plot(y=list_to_plot, figsize=(2*16,2*9),
                                   kind='line',
                                   linewidth=3, logy=log_flag,
                                   title=self.fNameData.rstrip('.csv') +
                                   fName_compl + suffix_name,
                                   colormap='tab20', fontsize=18)
        # Arrange a bit the plot
        if suffix_name == '-normalized':
            plot_df2T.set_ylabel("cumulated cases [% of pop.]", size=30)
        else:
            plot_df2T.set_ylabel("cumulated cases", size=30)
        plot_df2T.tick_params(axis='x', labelsize=24, size=20)
        plot_df2T.tick_params(axis='y', labelsize=24, size=20)
        plot_df2T.title.set(fontsize=36)
        plot_df2T.legend(fontsize=18)
        plot_df2T.axis(fontsize=18)

        # Extract figure handle for printing
        fig_df2T=plot_df2T.get_figure()
        # Print figure to png
        fig_df2T.savefig(self.pNameResults + self.fNameData.rstrip('.csv') +
            fName_compl + suffix_name + '.png')

        # Create plot, leveraging matplotlib, from pandas object
        if log_flag:
            fName_compl = '-gradient_log'
        else:
            fName_compl = '-gradient'
        plot_df2TG = self.df2TG.plot(y=list_to_plot, figsize=(2*16,2*9),
                                   kind='line',
                                   linewidth=3, logy=log_flag,
                                   title=self.fNameData.rstrip('.csv') +
                                   fName_compl + suffix_name,
                                   colormap='tab20', fontsize=18)
        # Arrange a bit the plot
        if suffix_name == '-normalized':
            plot_df2TG.set_ylabel("daily new cases [% of pop.]", size=30)
        else:
            plot_df2TG.set_ylabel("daily new cases", size=30)
            plot_df2TG.set_ylim(1, 100000)
        plot_df2TG.tick_params(axis='x', labelsize=24, size=20)
        plot_df2TG.tick_params(axis='y', labelsize=24, size=20)
        plot_df2TG.title.set(fontsize=36)
        plot_df2TG.legend(fontsize=18)
        plot_df2TG.axis(fontsize=18)

        # Extract figure handle for printing
        fig_df2TG=plot_df2TG.get_figure()
        # Print figure to png
        fig_df2TG.savefig(self.pNameResults + self.fNameData.rstrip('.csv') +
                fName_compl + suffix_name + '.png')

        # Create plot, leveraging matplotlib, from pandas object
        if log_flag:
            fName_compl = '-smooth1gradient_log'
        else:
            fName_compl = '-smooth1gradient'
        plot_df2TGS1 = self.df2TGS1.plot(y=list_to_plot, figsize=(2*16,2*9),
                                   kind='line',
                                   linewidth=3, logy=log_flag,
                                   title=self.fNameData.rstrip('.csv') +
                                   fName_compl + suffix_name,
                                   colormap='tab20', fontsize=18)
        # Arrange a bit the plot
        if suffix_name == '-normalized':
            plot_df2TGS1.set_ylabel("daily new cases [% of pop.]", size=30)
        else:
            plot_df2TGS1.set_ylabel("daily new cases", size=30)
            plot_df2TGS1.set_ylim(1, 100000)
        plot_df2TGS1.tick_params(axis='x', labelsize=24, size=20)
        plot_df2TGS1.tick_params(axis='y', labelsize=24, size=20)
        plot_df2TGS1.title.set(fontsize=36)
        plot_df2TGS1.legend(fontsize=18)
        plot_df2TGS1.axis(fontsize=18)

        # Extract figure handle for printing
        fig_df2TGS1=plot_df2TGS1.get_figure()
        # Print figure to png
        fig_df2TGS1.savefig(self.pNameResults + self.fNameData.rstrip('.csv') +
                fName_compl + suffix_name + '.png')

        # Create plot, leveraging matplotlib, from pandas object
        if log_flag:
            fName_compl = '-smooth2gradient_log'
        else:
            fName_compl = '-smooth2gradient'
        plot_df2TGS2 = self.df2TGS2.plot(y=list_to_plot, figsize=(2*16,2*9),
                                   kind='line',
                                   linewidth=3, logy=log_flag,
                                   title=self.fNameData.rstrip('.csv') +
                                   fName_compl + suffix_name,
                                   colormap='tab20', fontsize=18)
        # Arrange a bit the plot
        if suffix_name == '-normalized':
            plot_df2TGS2.set_ylabel("daily new cases [% of pop.]", size=30)
        else:
            plot_df2TGS2.set_ylabel("daily new cases", size=30)
            plot_df2TGS2.set_ylim(1, 100000)
        plot_df2TGS2.tick_params(axis='x', labelsize=24, size=20)
        plot_df2TGS2.tick_params(axis='y', labelsize=24, size=20)
        plot_df2TGS2.title.set(fontsize=36)
        plot_df2TGS2.legend(fontsize=18)
        plot_df2TGS2.axis(fontsize=18)

        # Extract figure handle for printing
        fig_df2TGS2=plot_df2TGS2.get_figure()
        # Print figure to png
        fig_df2TGS2.savefig(self.pNameResults + self.fNameData.rstrip('.csv') +
                fName_compl + suffix_name + '.png')
        
        # Create plot, leveraging matplotlib, from pandas object
        if log_flag:
            fName_compl = '-gradient2_log'
        else:
            fName_compl = '-gradient2'
        plot_df2TG2 = self.df2TG2.plot(y=list_to_plot, figsize=(2*16,2*9),
                                   kind='line',
                                   linewidth=3, logy=log_flag,
                                   title=self.fNameData.rstrip('.csv') +
                                   fName_compl + suffix_name,
                                   colormap='tab20', fontsize=18)
        # Arrange a bit the plot
        if suffix_name == '-normalized':
            plot_df2TG2.set_ylabel("daily new cases [% of pop.]", size=30)
        else:
            plot_df2TG2.set_ylabel("daily new cases", size=30)
            #plot_df2TG2.set_ylim(0, 100000)
        plot_df2TG2.tick_params(axis='x', labelsize=24, size=20)
        plot_df2TG2.tick_params(axis='y', labelsize=24, size=20)
        plot_df2TG2.title.set(fontsize=36)
        plot_df2TG2.legend(fontsize=18)
        plot_df2TG2.axis(fontsize=18)

        # Extract figure handle for printing
        fig_df2TG2=plot_df2TG2.get_figure()
        # Print figure to png
        fig_df2TG2.savefig(self.pNameResults + self.fNameData.rstrip('.csv') +
                fName_compl + suffix_name + '.png')
        return

def main(debug: bool=False) -> None:
    #Some constant configs
    pNameFilter = "./db/inputs/"
    fNameFilter = "list_countries_interest.csv"    
    pNameData = "./db/inputs/COVID-19/csse_covid_19_data/csse_covid_19_time_series/"
    
    # Creation of studies
    fNameData = "time_series_covid19_confirmed_global.csv"
    covid_confirmed_study2 = CovidContainer(pNameFilter, fNameFilter, pNameData, fNameData, debug)
    fNameData = "time_series_covid19_recovered_global.csv"
    covid_recovered_study2 = CovidContainer(pNameFilter, fNameFilter, pNameData, fNameData, debug)
    fNameData = "time_series_covid19_deaths_global.csv"
    covid_deaths_study2 = CovidContainer(pNameFilter, fNameFilter, pNameData, fNameData, debug)

    # A bit more of data transform
    covid_confirmed_study2.calculate_totals(area_name='Europe')
    covid_confirmed_study2.calculate_totals(area_name='Asia', country_name='China')
    covid_recovered_study2.calculate_totals(area_name='Europe')
    covid_recovered_study2.calculate_totals(area_name='Asia', country_name='China')
    covid_deaths_study2.calculate_totals(area_name='Europe')
    covid_deaths_study2.calculate_totals(area_name='Asia', country_name='China')

    covid_confirmed_study2.calculate_gradient()
    covid_recovered_study2.calculate_gradient()
    covid_deaths_study2.calculate_gradient()

    list_to_plot = ['Total-Asia-China', 'Total-Europe-All',
        'Europe-Italy-nan', 'Europe-Belgium-nan', 'Europe-Luxembourg-nan',
        'Europe-France-France', 'Europe-Germany-nan', 'Europe-Spain-nan',
        'Europe-Denmark-Denmark', 'Europe-United Kingdom-United Kingdom',
        'Europe-Switzerland-nan', 'Europe-Netherlands-Netherlands',
        'Asia-China-Hubei']
    list_to_plot = ['Total-Asia-China', 'Total-Europe-All',
        'Europe-Italy-nan', 'Europe-Belgium-nan', 'Europe-Luxembourg-nan',
        'Europe-France-nan', 'Europe-Germany-nan', 'Europe-Spain-nan',
        'Europe-Denmark-nan', 'Europe-United Kingdom-nan',
        'Europe-Switzerland-nan', 'Europe-Netherlands-nan', 
        'Asia-China-Hubei']
    print('Plotting covid_confirmed_study2 - ' +'Please wait ...')
    covid_confirmed_study2.plot_data(list_to_plot, log_flag=False)
    covid_confirmed_study2.plot_data(list_to_plot, log_flag=True)
    print('Plotting covid_deaths_study2 - ' +'Please wait ...')
    covid_recovered_study2.plot_data(list_to_plot, log_flag=False)
    covid_recovered_study2.plot_data(list_to_plot, log_flag=True)
    print('Plotting covid_deaths_study2 - ' +'Please wait ...')
    covid_deaths_study2.plot_data(list_to_plot, log_flag=False)
    covid_deaths_study2.plot_data(list_to_plot, log_flag=True)

    if debug:
        print(covid_confirmed_study2.df1)
        print(covid_confirmed_study2.df2T)
        print(covid_recovered_study2.df1)
        print(covid_recovered_study2.df2T)
        print(covid_deaths_study2.df1)
        print(covid_deaths_study2.df2T)
        print(covid_confirmed_study2.df2T['Total-Europe-All'])
        print(covid_confirmed_study2.df2T['Total-Asia-China'])
        print(covid_recovered_study2.df2T['Total-Europe-All'])
        print(covid_recovered_study2.df2T['Total-Asia-China'])
        print(covid_deaths_study2.df2T['Total-Europe-All'])
        print(covid_deaths_study2.df2T['Total-Asia-China'])
    
    # Dump data
    covid_confirmed_study2.dump_data()
    covid_recovered_study2.dump_data()
    covid_deaths_study2.dump_data()
    
    # Normalize
    covid_confirmed_study2.normalize_on_population()
    covid_recovered_study2.normalize_on_population()
    covid_deaths_study2.normalize_on_population()

    # Dump data
    covid_confirmed_study2.dump_data(flag_normalized=True)
    covid_recovered_study2.dump_data(flag_normalized=True)
    covid_deaths_study2.dump_data(flag_normalized=True)

    print('Plotting (normalized) covid_confirmed_study2 - ' +'Please wait ...')
    covid_confirmed_study2.plot_data(list_to_plot, log_flag=False, suffix_name='-normalized')
    covid_confirmed_study2.plot_data(list_to_plot, log_flag=True, suffix_name='-normalized')
    print('Plotting (normalized) covid_deaths_study2 - ' +'Please wait ...')
    covid_recovered_study2.plot_data(list_to_plot, log_flag=False, suffix_name='-normalized')
    covid_recovered_study2.plot_data(list_to_plot, log_flag=True, suffix_name='-normalized')
    print('Plotting (normalized) covid_deaths_study2 - ' +'Please wait ...')
    covid_deaths_study2.plot_data(list_to_plot, log_flag=False, suffix_name='-normalized')
    covid_deaths_study2.plot_data(list_to_plot, log_flag=True, suffix_name='-normalized')
    
    # Denormalize
    covid_confirmed_study2.normalize_on_population(False)
    covid_recovered_study2.normalize_on_population(False)
    covid_deaths_study2.normalize_on_population(False)

if __name__ == '__main__':
    #warnings.filterwarnings('error')
    #np.seterr(all='warn')
    np.errstate(divide='raise')
    main(False)