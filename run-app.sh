cd db
cd inputs
if [ -d "./COVID-19/" ] 
then
    cd COVID-19
    git pull
    cd ..
else
    #git clone https://github.com/CSSEGISandData/COVID-19.git
    # Dirty betty:
    env GIT_SSL_NO_VERIFY=true git clone https://github.com/CSSEGISandData/COVID-19.git
fi
cd ..
cd ..
ECHO Activate Anaconda prompt and launch sequentially the python 3 script

env/bin/python3 ./src/cov-analyser.py
