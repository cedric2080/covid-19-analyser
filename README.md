Covid-19 Results analyser

 | Necessitates
-|--------------
- Anaconda 3 distribution of python 3 (https://repo.anaconda.com/archive/Anaconda3-2020.02-Windows-x86_64.exe)
- or equivalent with:
  * numpy (distributed by default)
  * pandas (may need a "conda install pandas" in the anaconda prompt or "pip install pandas")
  * git (distributed by default with anaconda) to update automatically the source of data repository

 | How to launch
-|--------------
Windows: double click "launch.bat" in root of project directory, that's all (should be because not tested ...).
Linux: from root of project directory
   1- First run: "./run-deploy.sh"
   2- Next runs: "./run-app.sh"

 | Docs
-|-----
https://ourcodingclub.github.io/tutorials/python-intro/index.html
https://ourcodingclub.github.io/tutorials/pandas-time-series/
https://ourcodingclub.github.io/tutorials/pandas-python-intro/
http://eric.univ-lyon2.fr/~ricco/tanagra/fichiers/fr_Tanagra_Data_Manipulation_Pandas.pdf

DO NOT USE SPYDER, in my experience, it is far better to use:
- a real IDE + command line
or
- jupyterlabs if you really want something that is data or science and maybe business
oriented

https://voussoir.net/writing/python/ifnamemain