# Django server - Done manually to initiate coding
#../env/bin/django-admin startproject covid_19_platform

# Apps - Done manually to initiate coding
#cd covid_19_platform
#../env/bin/python3 manage.py startapp polls

## Create the superuser
../env/bin/python3 manage.py createsuperuser

# Create tables
cd covid_19_platform
## Initial migration
../env/bin/python3 manage.py migrate

cd ..